# Prueba Front-End AND
> Raquel Salcedo Mejía | rqlsm95@gmail.com


## Ejecutar proyecto

Ejecutar `ng serve` en la terminal. Abrir  `http://localhost:4200/` en el navegador.

## Secciones implementadas

Se implementaron las 4 secciones:
- Trámites más usados
- Queremos conocer su opinión
- Infórmate
- Otros temas de interés

## Configurar contenido de las secciones

Todo el contenido configurable se encuentra en el archivo `content.json` el cual puede ser consultado en la siguiente ruta`src/assets/json/content.json`