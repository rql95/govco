import { Component, OnInit } from '@angular/core';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-temas',
  templateUrl: './temas.component.html',
  styleUrls: ['./temas.component.css']
})
export class TemasComponent implements OnInit {

  temas: any = content.temas;
  constructor() { }

  ngOnInit(): void {
  }

}
