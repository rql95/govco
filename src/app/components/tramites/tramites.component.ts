import { Component, OnInit } from '@angular/core';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css']
})
export class TramitesComponent implements OnInit {

  tramites: any = content.tramites;

  constructor() { }

  ngOnInit(): void {
  }

}
