import { Component, OnInit } from '@angular/core';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-tramites-top',
  templateUrl: './tramites-top.component.html',
  styleUrls: ['./tramites-top.component.css']
})
export class TramitesTopComponent implements OnInit {
  tramitesTop: any = content.tramitesTop;

  constructor() { }

  ngOnInit(): void {
  }

}
