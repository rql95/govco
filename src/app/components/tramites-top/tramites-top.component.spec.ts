import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesTopComponent } from './tramites-top.component';

describe('TramitesTopComponent', () => {
  let component: TramitesTopComponent;
  let fixture: ComponentFixture<TramitesTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
