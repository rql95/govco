import { Component, OnInit } from '@angular/core';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-opiniones',
  templateUrl: './opiniones.component.html',
  styleUrls: ['./opiniones.component.css']
})
export class OpinionesComponent implements OnInit {

  opiniones: any = content.opiniones;
  constructor() { }

  ngOnInit(): void {
  }

}
