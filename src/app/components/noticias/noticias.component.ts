import { Component, OnInit } from '@angular/core';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  noticias: any = content.noticias;
  constructor() { }

  ngOnInit(): void {
  }

}
