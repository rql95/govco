import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TramitesComponent } from './components/tramites/tramites.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { TemasComponent } from './components/temas/temas.component';
import { OpinionesComponent } from './components/opiniones/opiniones.component';
import { TramitesTopComponent } from './components/tramites-top/tramites-top.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    TramitesComponent,
    NoticiasComponent,
    TemasComponent,
    OpinionesComponent,
    TramitesTopComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
