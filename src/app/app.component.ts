import { Component } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import content from 'src/assets/json/content.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'GovCo';
  banner: any = content.banner;
  tramites: any = content.tramites;
  tramitesTop: any = content.tramitesTop;
  opiniones: any = content.opiniones;
  noticias: any = content.noticias;
  temas: any = content.temas;
}